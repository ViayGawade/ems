--
-- PostgreSQL database dump
--

-- Dumped from database version 12.0
-- Dumped by pg_dump version 12.0

-- Started on 2019-10-31 18:23:37

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE "EMS";
--
-- TOC entry 2901 (class 1262 OID 16394)
-- Name: EMS; Type: DATABASE; Schema: -; Owner: vijay
--

CREATE DATABASE "EMS" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_India.1252' LC_CTYPE = 'English_India.1252';


ALTER DATABASE "EMS" OWNER TO vijay;

\connect "EMS"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2902 (class 0 OID 0)
-- Dependencies: 2901
-- Name: DATABASE "EMS"; Type: COMMENT; Schema: -; Owner: vijay
--

COMMENT ON DATABASE "EMS" IS 'Employees Management System';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 211 (class 1259 OID 16434)
-- Name: compliance; Type: TABLE; Schema: public; Owner: vijay
--

CREATE TABLE public.compliance (
    id integer NOT NULL,
    "regulationType" character varying NOT NULL,
    details character varying(255) NOT NULL,
    "departmentID" integer NOT NULL,
    "createDate" timestamp with time zone NOT NULL
);


ALTER TABLE public.compliance OWNER TO vijay;

--
-- TOC entry 210 (class 1259 OID 16432)
-- Name: compliance_departmentID_seq; Type: SEQUENCE; Schema: public; Owner: vijay
--

CREATE SEQUENCE public."compliance_departmentID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."compliance_departmentID_seq" OWNER TO vijay;

--
-- TOC entry 2903 (class 0 OID 0)
-- Dependencies: 210
-- Name: compliance_departmentID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vijay
--

ALTER SEQUENCE public."compliance_departmentID_seq" OWNED BY public.compliance."departmentID";


--
-- TOC entry 209 (class 1259 OID 16430)
-- Name: compliance_id_seq; Type: SEQUENCE; Schema: public; Owner: vijay
--

CREATE SEQUENCE public.compliance_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.compliance_id_seq OWNER TO vijay;

--
-- TOC entry 2904 (class 0 OID 0)
-- Dependencies: 209
-- Name: compliance_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vijay
--

ALTER SEQUENCE public.compliance_id_seq OWNED BY public.compliance.id;


--
-- TOC entry 203 (class 1259 OID 16397)
-- Name: department; Type: TABLE; Schema: public; Owner: vijay
--

CREATE TABLE public.department (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.department OWNER TO vijay;

--
-- TOC entry 202 (class 1259 OID 16395)
-- Name: department_id_seq; Type: SEQUENCE; Schema: public; Owner: vijay
--

CREATE SEQUENCE public.department_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.department_id_seq OWNER TO vijay;

--
-- TOC entry 2905 (class 0 OID 0)
-- Dependencies: 202
-- Name: department_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vijay
--

ALTER SEQUENCE public.department_id_seq OWNED BY public.department.id;


--
-- TOC entry 206 (class 1259 OID 16407)
-- Name: employee; Type: TABLE; Schema: public; Owner: vijay
--

CREATE TABLE public.employee (
    id integer NOT NULL,
    "firstName" character varying(80) NOT NULL,
    "lastName" character varying(80) NOT NULL,
    "dateOfBirth" date,
    email character varying(100),
    "departmentID" integer NOT NULL
);


ALTER TABLE public.employee OWNER TO vijay;

--
-- TOC entry 205 (class 1259 OID 16405)
-- Name: employee_departmentID_seq; Type: SEQUENCE; Schema: public; Owner: vijay
--

CREATE SEQUENCE public."employee_departmentID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."employee_departmentID_seq" OWNER TO vijay;

--
-- TOC entry 2906 (class 0 OID 0)
-- Dependencies: 205
-- Name: employee_departmentID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vijay
--

ALTER SEQUENCE public."employee_departmentID_seq" OWNED BY public.employee."departmentID";


--
-- TOC entry 204 (class 1259 OID 16403)
-- Name: employee_id_seq; Type: SEQUENCE; Schema: public; Owner: vijay
--

CREATE SEQUENCE public.employee_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.employee_id_seq OWNER TO vijay;

--
-- TOC entry 2907 (class 0 OID 0)
-- Dependencies: 204
-- Name: employee_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vijay
--

ALTER SEQUENCE public.employee_id_seq OWNED BY public.employee.id;


--
-- TOC entry 217 (class 1259 OID 16480)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: vijay
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO vijay;

--
-- TOC entry 218 (class 1259 OID 16482)
-- Name: hibernate_sequences; Type: TABLE; Schema: public; Owner: vijay
--

CREATE TABLE public.hibernate_sequences (
    sequence_name character varying(255) NOT NULL,
    next_val bigint
);


ALTER TABLE public.hibernate_sequences OWNER TO vijay;

--
-- TOC entry 208 (class 1259 OID 16421)
-- Name: loginMaster; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."loginMaster" (
    "userID" integer NOT NULL,
    password character varying(255) NOT NULL,
    role character varying(10) NOT NULL
);


ALTER TABLE public."loginMaster" OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 16419)
-- Name: loginMaster_userID_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."loginMaster_userID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."loginMaster_userID_seq" OWNER TO postgres;

--
-- TOC entry 2908 (class 0 OID 0)
-- Dependencies: 207
-- Name: loginMaster_userID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."loginMaster_userID_seq" OWNED BY public."loginMaster"."userID";


--
-- TOC entry 216 (class 1259 OID 16457)
-- Name: statusReport; Type: TABLE; Schema: public; Owner: vijay
--

CREATE TABLE public."statusReport" (
    id integer NOT NULL,
    "complianceID" integer NOT NULL,
    "employeeID" integer NOT NULL,
    comment character varying(255) NOT NULL,
    "createDate" timestamp with time zone NOT NULL,
    "departmentID" integer NOT NULL
);


ALTER TABLE public."statusReport" OWNER TO vijay;

--
-- TOC entry 213 (class 1259 OID 16451)
-- Name: statusReport_complianceID_seq; Type: SEQUENCE; Schema: public; Owner: vijay
--

CREATE SEQUENCE public."statusReport_complianceID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."statusReport_complianceID_seq" OWNER TO vijay;

--
-- TOC entry 2909 (class 0 OID 0)
-- Dependencies: 213
-- Name: statusReport_complianceID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vijay
--

ALTER SEQUENCE public."statusReport_complianceID_seq" OWNED BY public."statusReport"."complianceID";


--
-- TOC entry 215 (class 1259 OID 16455)
-- Name: statusReport_departmentID_seq; Type: SEQUENCE; Schema: public; Owner: vijay
--

CREATE SEQUENCE public."statusReport_departmentID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."statusReport_departmentID_seq" OWNER TO vijay;

--
-- TOC entry 2910 (class 0 OID 0)
-- Dependencies: 215
-- Name: statusReport_departmentID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vijay
--

ALTER SEQUENCE public."statusReport_departmentID_seq" OWNED BY public."statusReport"."departmentID";


--
-- TOC entry 214 (class 1259 OID 16453)
-- Name: statusReport_employeeID_seq; Type: SEQUENCE; Schema: public; Owner: vijay
--

CREATE SEQUENCE public."statusReport_employeeID_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."statusReport_employeeID_seq" OWNER TO vijay;

--
-- TOC entry 2911 (class 0 OID 0)
-- Dependencies: 214
-- Name: statusReport_employeeID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vijay
--

ALTER SEQUENCE public."statusReport_employeeID_seq" OWNED BY public."statusReport"."employeeID";


--
-- TOC entry 212 (class 1259 OID 16449)
-- Name: statusReport_id_seq; Type: SEQUENCE; Schema: public; Owner: vijay
--

CREATE SEQUENCE public."statusReport_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."statusReport_id_seq" OWNER TO vijay;

--
-- TOC entry 2912 (class 0 OID 0)
-- Dependencies: 212
-- Name: statusReport_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vijay
--

ALTER SEQUENCE public."statusReport_id_seq" OWNED BY public."statusReport".id;


--
-- TOC entry 2733 (class 2604 OID 16437)
-- Name: compliance id; Type: DEFAULT; Schema: public; Owner: vijay
--

ALTER TABLE ONLY public.compliance ALTER COLUMN id SET DEFAULT nextval('public.compliance_id_seq'::regclass);


--
-- TOC entry 2734 (class 2604 OID 16438)
-- Name: compliance departmentID; Type: DEFAULT; Schema: public; Owner: vijay
--

ALTER TABLE ONLY public.compliance ALTER COLUMN "departmentID" SET DEFAULT nextval('public."compliance_departmentID_seq"'::regclass);


--
-- TOC entry 2728 (class 2604 OID 16400)
-- Name: department id; Type: DEFAULT; Schema: public; Owner: vijay
--

ALTER TABLE ONLY public.department ALTER COLUMN id SET DEFAULT nextval('public.department_id_seq'::regclass);


--
-- TOC entry 2729 (class 2604 OID 16410)
-- Name: employee id; Type: DEFAULT; Schema: public; Owner: vijay
--

ALTER TABLE ONLY public.employee ALTER COLUMN id SET DEFAULT nextval('public.employee_id_seq'::regclass);


--
-- TOC entry 2730 (class 2604 OID 16411)
-- Name: employee departmentID; Type: DEFAULT; Schema: public; Owner: vijay
--

ALTER TABLE ONLY public.employee ALTER COLUMN "departmentID" SET DEFAULT nextval('public."employee_departmentID_seq"'::regclass);


--
-- TOC entry 2731 (class 2604 OID 16424)
-- Name: loginMaster userID; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."loginMaster" ALTER COLUMN "userID" SET DEFAULT nextval('public."loginMaster_userID_seq"'::regclass);


--
-- TOC entry 2735 (class 2604 OID 16460)
-- Name: statusReport id; Type: DEFAULT; Schema: public; Owner: vijay
--

ALTER TABLE ONLY public."statusReport" ALTER COLUMN id SET DEFAULT nextval('public."statusReport_id_seq"'::regclass);


--
-- TOC entry 2736 (class 2604 OID 16461)
-- Name: statusReport complianceID; Type: DEFAULT; Schema: public; Owner: vijay
--

ALTER TABLE ONLY public."statusReport" ALTER COLUMN "complianceID" SET DEFAULT nextval('public."statusReport_complianceID_seq"'::regclass);


--
-- TOC entry 2737 (class 2604 OID 16462)
-- Name: statusReport employeeID; Type: DEFAULT; Schema: public; Owner: vijay
--

ALTER TABLE ONLY public."statusReport" ALTER COLUMN "employeeID" SET DEFAULT nextval('public."statusReport_employeeID_seq"'::regclass);


--
-- TOC entry 2738 (class 2604 OID 16463)
-- Name: statusReport departmentID; Type: DEFAULT; Schema: public; Owner: vijay
--

ALTER TABLE ONLY public."statusReport" ALTER COLUMN "departmentID" SET DEFAULT nextval('public."statusReport_departmentID_seq"'::regclass);


--
-- TOC entry 2888 (class 0 OID 16434)
-- Dependencies: 211
-- Data for Name: compliance; Type: TABLE DATA; Schema: public; Owner: vijay
--



--
-- TOC entry 2880 (class 0 OID 16397)
-- Dependencies: 203
-- Data for Name: department; Type: TABLE DATA; Schema: public; Owner: vijay
--

INSERT INTO public.department (id, name) VALUES (1, 'Interdisciplinary School of Scientific Computing');
INSERT INTO public.department (id, name) VALUES (2, 'Centre for Information and Network Security (CINS)');
INSERT INTO public.department (id, name) VALUES (3, 'Yashwantrao Chavan National Centre of International Security and Defense Analysi');
INSERT INTO public.department (id, name) VALUES (4, 'Sanskrit and Prakrit Languages');
INSERT INTO public.department (id, name) VALUES (5, 'SCHOOL OF BASIC MEDICAL SCIENCES');
INSERT INTO public.department (id, name) VALUES (6, 'Defence and Strategic Studies');
INSERT INTO public.department (id, name) VALUES (7, 'Skill Development Center');
INSERT INTO public.department (id, name) VALUES (8, 'Physics');
INSERT INTO public.department (id, name) VALUES (9, 'Mathemetics & Stats');
INSERT INTO public.department (id, name) VALUES (10, 'Computer Modeling & Simulation');
INSERT INTO public.department (id, name) VALUES (13, 'avcx');
INSERT INTO public.department (id, name) VALUES (14, 'asass');


--
-- TOC entry 2883 (class 0 OID 16407)
-- Dependencies: 206
-- Data for Name: employee; Type: TABLE DATA; Schema: public; Owner: vijay
--



--
-- TOC entry 2895 (class 0 OID 16482)
-- Dependencies: 218
-- Data for Name: hibernate_sequences; Type: TABLE DATA; Schema: public; Owner: vijay
--

INSERT INTO public.hibernate_sequences (sequence_name, next_val) VALUES ('default', 3);


--
-- TOC entry 2885 (class 0 OID 16421)
-- Dependencies: 208
-- Data for Name: loginMaster; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2893 (class 0 OID 16457)
-- Dependencies: 216
-- Data for Name: statusReport; Type: TABLE DATA; Schema: public; Owner: vijay
--



--
-- TOC entry 2913 (class 0 OID 0)
-- Dependencies: 210
-- Name: compliance_departmentID_seq; Type: SEQUENCE SET; Schema: public; Owner: vijay
--

SELECT pg_catalog.setval('public."compliance_departmentID_seq"', 1, false);


--
-- TOC entry 2914 (class 0 OID 0)
-- Dependencies: 209
-- Name: compliance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vijay
--

SELECT pg_catalog.setval('public.compliance_id_seq', 1, false);


--
-- TOC entry 2915 (class 0 OID 0)
-- Dependencies: 202
-- Name: department_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vijay
--

SELECT pg_catalog.setval('public.department_id_seq', 14, true);


--
-- TOC entry 2916 (class 0 OID 0)
-- Dependencies: 205
-- Name: employee_departmentID_seq; Type: SEQUENCE SET; Schema: public; Owner: vijay
--

SELECT pg_catalog.setval('public."employee_departmentID_seq"', 1, false);


--
-- TOC entry 2917 (class 0 OID 0)
-- Dependencies: 204
-- Name: employee_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vijay
--

SELECT pg_catalog.setval('public.employee_id_seq', 1, false);


--
-- TOC entry 2918 (class 0 OID 0)
-- Dependencies: 217
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: vijay
--

SELECT pg_catalog.setval('public.hibernate_sequence', 2, true);


--
-- TOC entry 2919 (class 0 OID 0)
-- Dependencies: 207
-- Name: loginMaster_userID_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."loginMaster_userID_seq"', 1, false);


--
-- TOC entry 2920 (class 0 OID 0)
-- Dependencies: 213
-- Name: statusReport_complianceID_seq; Type: SEQUENCE SET; Schema: public; Owner: vijay
--

SELECT pg_catalog.setval('public."statusReport_complianceID_seq"', 1, false);


--
-- TOC entry 2921 (class 0 OID 0)
-- Dependencies: 215
-- Name: statusReport_departmentID_seq; Type: SEQUENCE SET; Schema: public; Owner: vijay
--

SELECT pg_catalog.setval('public."statusReport_departmentID_seq"', 1, false);


--
-- TOC entry 2922 (class 0 OID 0)
-- Dependencies: 214
-- Name: statusReport_employeeID_seq; Type: SEQUENCE SET; Schema: public; Owner: vijay
--

SELECT pg_catalog.setval('public."statusReport_employeeID_seq"', 1, false);


--
-- TOC entry 2923 (class 0 OID 0)
-- Dependencies: 212
-- Name: statusReport_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vijay
--

SELECT pg_catalog.setval('public."statusReport_id_seq"', 1, false);


--
-- TOC entry 2744 (class 2606 OID 16443)
-- Name: compliance compliance_pkey; Type: CONSTRAINT; Schema: public; Owner: vijay
--

ALTER TABLE ONLY public.compliance
    ADD CONSTRAINT compliance_pkey PRIMARY KEY (id);


--
-- TOC entry 2740 (class 2606 OID 16402)
-- Name: department department_pkey; Type: CONSTRAINT; Schema: public; Owner: vijay
--

ALTER TABLE ONLY public.department
    ADD CONSTRAINT department_pkey PRIMARY KEY (id);


--
-- TOC entry 2742 (class 2606 OID 16413)
-- Name: employee employee_pkey; Type: CONSTRAINT; Schema: public; Owner: vijay
--

ALTER TABLE ONLY public.employee
    ADD CONSTRAINT employee_pkey PRIMARY KEY (id);


--
-- TOC entry 2746 (class 2606 OID 16486)
-- Name: hibernate_sequences hibernate_sequences_pkey; Type: CONSTRAINT; Schema: public; Owner: vijay
--

ALTER TABLE ONLY public.hibernate_sequences
    ADD CONSTRAINT hibernate_sequences_pkey PRIMARY KEY (sequence_name);


--
-- TOC entry 2732 (class 2606 OID 16479)
-- Name: loginMaster roleType; Type: CHECK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE public."loginMaster"
    ADD CONSTRAINT "roleType" CHECK (((role)::text = ANY ((ARRAY['admin'::character varying, 'user'::character varying])::text[]))) NOT VALID;


--
-- TOC entry 2750 (class 2606 OID 16464)
-- Name: statusReport complianceID; Type: FK CONSTRAINT; Schema: public; Owner: vijay
--

ALTER TABLE ONLY public."statusReport"
    ADD CONSTRAINT "complianceID" FOREIGN KEY ("complianceID") REFERENCES public.compliance(id);


--
-- TOC entry 2749 (class 2606 OID 16444)
-- Name: compliance compliance_departmentID_fkey; Type: FK CONSTRAINT; Schema: public; Owner: vijay
--

ALTER TABLE ONLY public.compliance
    ADD CONSTRAINT "compliance_departmentID_fkey" FOREIGN KEY ("departmentID") REFERENCES public.department(id);


--
-- TOC entry 2747 (class 2606 OID 16414)
-- Name: employee departmentID; Type: FK CONSTRAINT; Schema: public; Owner: vijay
--

ALTER TABLE ONLY public.employee
    ADD CONSTRAINT "departmentID" FOREIGN KEY ("departmentID") REFERENCES public.department(id);


--
-- TOC entry 2752 (class 2606 OID 16474)
-- Name: statusReport departmentID; Type: FK CONSTRAINT; Schema: public; Owner: vijay
--

ALTER TABLE ONLY public."statusReport"
    ADD CONSTRAINT "departmentID" FOREIGN KEY ("departmentID") REFERENCES public.department(id);


--
-- TOC entry 2751 (class 2606 OID 16469)
-- Name: statusReport employeeID; Type: FK CONSTRAINT; Schema: public; Owner: vijay
--

ALTER TABLE ONLY public."statusReport"
    ADD CONSTRAINT "employeeID" FOREIGN KEY ("employeeID") REFERENCES public.employee(id);


--
-- TOC entry 2748 (class 2606 OID 16425)
-- Name: loginMaster userID; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."loginMaster"
    ADD CONSTRAINT "userID" FOREIGN KEY ("userID") REFERENCES public.employee(id);


-- Completed on 2019-10-31 18:23:39

--
-- PostgreSQL database dump complete
--

