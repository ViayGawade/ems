package com.vijay.ems.display;

public class OperationResult {
	boolean status;
	String action;
	String message;
	
	
	public OperationResult(boolean status, String action, String message) {
		super();
		this.status = status;
		this.action = action;
		this.message = message;
	}
	public OperationResult() {
		this.status = true;
		this.action = "not action";
		this.message = "no message";
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getJSON_String() {
		if(this.action==null)
			return "";
		else
			return "{\"status\":\""+this.status+"\",\"action\":\""+this.action+"\",\"message\":\""+this.message+"\"}";
	}
}
