package com.vijay.ems.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LoginController {
	@GetMapping("login")
	 public String showLoginPage() {
		return "login/login";
	}
	
	@PostMapping("login")
	public String authenticate(String username,String password,String usertype) {
		if(username.equals("abc@gmail.com") && password.equals("admin@123") && usertype.equals("admin"))
			return "redirect:/";
		else
			return "login/login";
	}
}
