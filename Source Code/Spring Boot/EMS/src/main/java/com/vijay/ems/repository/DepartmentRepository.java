package com.vijay.ems.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vijay.ems.model.Department;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {
	
}
