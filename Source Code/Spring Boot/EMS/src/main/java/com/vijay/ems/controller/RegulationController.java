package com.vijay.ems.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("regulations")
public class RegulationController {
	@GetMapping({"","/list"})
	public String showRegulationList() {
		return "admin/regulations";
	}
}
