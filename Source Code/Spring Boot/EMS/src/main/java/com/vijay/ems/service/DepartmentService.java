package com.vijay.ems.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vijay.ems.display.OperationResult;
import com.vijay.ems.model.Department;
import com.vijay.ems.repository.DepartmentRepository;

@Service
public class DepartmentService {
	@Autowired
	private DepartmentRepository departmentRepository;
	
	public List<Department> getDepartmentList(){		
		return departmentRepository.findAll();
	}

	public OperationResult addDepartment(@Valid Department department) {
		Department dept=departmentRepository.save(department);
		if(dept!=null)
			return new OperationResult(true, "addDepartment", "Department saved with ID="+dept.id);
		else
			return new OperationResult(false, "addDepartment", "Some error occured durring adding Departpent !.  try again ...");
	}

	public OperationResult deleteDepartment(long departmetID) {
		try {
			departmentRepository.deleteById(departmetID);
			return new OperationResult(true, "deleteDepartment", "Department deleted. ");
		} catch (IllegalArgumentException e) {
			return new OperationResult(false, "deleteDepartment", "Departpent ID not found !  try again ...");
		}catch (Exception e) {
			return new OperationResult(false, "deleteDepartment", "Some error occured durring deleting Departpent !.  try again ...");
		}
	}
}
