package com.vijay.ems.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {
	@GetMapping("/msg")
	@ResponseBody
	public String welcomeMSG() {
		return "Welcome to EMS !";
	}
	
	@GetMapping(value= {"/welcome",""})
	public String index() {
		return "admin/home";
	}
}
