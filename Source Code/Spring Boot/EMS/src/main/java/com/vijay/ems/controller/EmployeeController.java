package com.vijay.ems.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("employees")
public class EmployeeController {
	@GetMapping({"","list"})
	public String showEmployeeList(@RequestParam(defaultValue = "1") int page) {
		System.out.println(page);
		return "admin/employees";
	}
}
