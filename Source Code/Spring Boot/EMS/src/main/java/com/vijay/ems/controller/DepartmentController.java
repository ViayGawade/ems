package com.vijay.ems.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.vijay.ems.display.OperationResult;
import com.vijay.ems.model.Department;
import com.vijay.ems.service.DepartmentService;

@Controller
@RequestMapping("departments")
public class DepartmentController {
	@Autowired
	private DepartmentService departmentService;
	private OperationResult operationResult=new OperationResult();
	
	@GetMapping({"","list"})
	public String showDepartmentList(ModelMap departmentModel) {
		departmentModel.addAttribute("department", new Department());
		departmentModel.addAttribute("actionResult", operationResult.getJSON_String());
		operationResult=new OperationResult();	
		
		return "admin/departments";
	}
	
	@PostMapping("add-department")
	public String addDepartment(@Valid @ModelAttribute("department") Department department, BindingResult result) {
		if(result.hasErrors())
			return "admin/departments";
		operationResult=departmentService.addDepartment(department);
		return "redirect:/departments";
	}
	
	@PostMapping("delete-department")
	public String deleteDepartment(@RequestParam int departmetID) {
		operationResult=departmentService.deleteDepartment(departmetID);
		return "redirect:/departments"; 
	}
	
	@ModelAttribute("departmentList")
	public List<Department> getDepartmentList(){
		 return departmentService.getDepartmentList();
	}
}
