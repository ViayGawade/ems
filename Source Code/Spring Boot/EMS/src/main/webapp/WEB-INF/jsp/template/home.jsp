<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport"
		content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<link rel="icon" href="images/site-logos/EMS2.jpg" />
	
	<title>Employee Management System</title>
	
	<!-- Bootstrap CDN  -->
	<link href="webjars/bootstrap/4.3.1/css/bootstrap.min.css"
		rel="stylesheet" />
	<script src="webjars/jquery/3.4.1/jquery.min.js"></script>
	<script src="webjars/bootstrap/4.3.1/js/bootstrap.min.js"></script>

</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand mb-0 h1" href="#"> <img
			src="images/site-logos/site.png" width="30" height="30"
			class="d-inline-block align-top" alt="" /> EMS
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarTogglerDemo03"
			aria-controls="navbarTogglerDemo03" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarTogglerDemo03">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="#">Home <span class="sr-only">(current)</span>
				</a>
				</li>
				<li class="nav-item"><a class="nav-link" href="#">Regulations/Legislation</a>
				</li>
				<li class="nav-item"><a class="nav-link" href="#">Departments</a>
				</li>
				<li class="nav-item"><a class="nav-link" href="#">Employee</a>
				</li>
			</ul>

			<div class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
					role="button" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> <img
					src="images/site-logos/user.png" width="30" height="30"
					class="d-inline-block align-self-xl-center" alt="" /> User
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown"
					style="margin-left: -80px;">
					<a class="dropdown-item" href="#">Edit Profile</a> <a
						class="dropdown-item" href="#">Change Password</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="#">Log Out</a>
				</div>
			</div>
		</div>
	</nav>


</body>
</html>
