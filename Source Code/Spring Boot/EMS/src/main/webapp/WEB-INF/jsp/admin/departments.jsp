<%@ include file="common/taglibs.jspf" %>

<%@ include file="common/header.jspf"%>
<title>Departments</title>
<style>
.modalError {
	width: 100%;
	margin-top: .25rem;
	font-size: 80%;
	color: #dc3545;
	display: block;
}

</style>
</head>
<%@ include file="common/navigation.jspf"%>


<!-- START Main Body -->
<div class="pageContent">
	<h2 style="text-align: center;">List of Depatments</h2>
	<br>
	<div class="row" style="float: right; margin-right: 10px;">
		<button type="button" class="btn btn-success btn-lg"
			data-toggle="modal" data-target="#departmentModal" data-title="Add">New
			Department</button>
	</div>
	<br>
	<br>
	<br>
	<div class="row table-responsive">
		<table class="table table-bordered table-hover text-center" style="width: 60%; margin: 0 auto !important;">
			<thead class="thead-dark">
				<tr>
					<th scope="col" width="10%">ID</th>
					<th scope="col" width="40%">Department Name</th>
					<th scope="col" width=10%">Action</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${departmentList}" var="department">
					<tr>
					<td>${department.id}</td>
					<td>${department.name}</td>
					<td>
						<button type="submit" form="deleteDepartmentForm" class="btn btn-danger btn-sm" onclick="addDepartmentID(${department.id});">
							<i class="fa fa-trash"></i>&nbsp;Delete
						</button>
					</td>
				</tr>
				</c:forEach>				
			</tbody>
		</table>
		<form action="departments/delete-department" method="post" id="deleteDepartmentForm">
			<input type="hidden" name="departmetID" value="0">
		</form>
	</div>

	<!-- Department Modal -->
	<div class="modal fade" id="departmentModal" tabindex="-1"
		role="dialog" aria-labelledby="departmentModal" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<form:form method="post" action="departments/add-department" modelAttribute="department" class="needs-validation addDepartmentForm" novalidate="true">			
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLongTitle">Add Department</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">

						<div class="form-row">
							<div class="form-group col-md-10">
								<form:label path="id">Department ID:</form:label>
								<form:input path="id"  class="form-control" required="required" value="0" readonly="true" />
								<form:errors path="id" cssClass="modalError" />								
							</div>

						</div>
						<br>
						<div class="form-row">
							<div class="form-group col-md-12">
								<form:label path="name">Department Name:</form:label>
								<form:input path="name"  class="form-control" required="required" placeholder="Enter Department Name" onblur="validateDepartmentName($(this));" onkeyup="validateDepartmentName($(this));"/>
								
								<small id="departmentNameHelp" class="form-text text-muted">Enter Full Department Name</small>
								<form:errors path="name" cssClass="modalError" />							
								
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-tooltip">Please provide a valid Department Name with size between <br>3 to 80 characters long.</div>
						</div>

					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-success">Save</button>
					</div>
				</div>			
			</form:form>
		</div>
	</div>
	<!-- End Of Department Modal -->



	<script
		src="${pageContext.request.contextPath}/custom/js/department/department.js"></script>

	<script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function () {
            'use strict';
            window.addEventListener('load', function () {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function (form) {
                    form.addEventListener('submit', function (event) {
                        if (validateAllFormField()==false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        else
                        	form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script> 

	<!-- Validator.js -->
	<script
		src="${pageContext.request.contextPath}/custom/js/validations/admin/department.js"></script>

	<!-- Display Alert Box -->
	<script type="text/javascript">
		$(document).ready(function(){
			var operationResult=JSON.parse('${actionResult}');
			
			if(operationResult.action=='addDepartment'){                                	
             	if(operationResult.status=='true'){	                                	
         			swal("Successfully Added !",operationResult.message , "success");	                                    
             	}
             	else if(operationResult.status=='false'){
             		swal("Failed to add Department !", operationResult.message, "error");
            	}
         	}
			else if(operationResult.action=='deleteDepartment'){                                	
             	if(operationResult.status=='true'){	                                	
         			swal("Successfully Deleted !",operationResult.message , "success");	                                    
             	}
             	else if(operationResult.status=='false'){
             		swal("Failed to delete Department !", operationResult.message, "error");
            	}
         	}
		});   	
	</script>

</div>
<!-- END Main Body -->

<%@ include file="common/footer.jspf"%>