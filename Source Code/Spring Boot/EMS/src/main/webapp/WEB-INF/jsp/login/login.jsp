<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<meta name="description" content="" />
<meta name="author" content="" />
<link rel="icon" href="images/site-logos/EMS2.jpg" />

<title>Employee Management System</title>


<!-- Bootstrap CDN  -->
<link href="webjars/bootstrap/4.3.1/css/bootstrap.min.css"
	rel="stylesheet" />

<!-- Custom styles for this template -->
<link href="custom/css/login/floating-labels.css" rel="stylesheet" />

<style>
input[name="usertype"] {
	margin-right: 20px;
	margin-left: 50px;
}

.userTypeGrp {
	margin-left: 40px;
	margin-right: 40px;
	font-size: 25px !important;
	font-family: Georgia !important;
}
</style>
</head>

<body>
	<form class="form-signin" action="login" method="POST">
		<div class="text-center mb-4">
			<img class="mb-4" src="images/site-logos/EMS.jpg" alt="EMS Logo"
				width="150" height="90" />
			<h1 class="h3 mb-3 font-weight-normal">Login Page</h1>
		</div>

		<div class="row userTypeGrp">
			<dic class="col-6 align-content-center">
			<div class="custom-control custom-radio ">
				<input type="radio" class="custom-control-input" id="usertypeAdmin"
					name="usertype" value="admin" /> <label
					class="custom-control-label" for="usertypeAdmin">Admin</label>
			</div>
			</dic>
			<div class="col-6">
				<div class="custom-control custom-radio">
					<input type="radio" class="custom-control-input" id="usertypeUser"
						name="usertype" value="user" checked /> <label
						class="custom-control-label" for="usertypeUser">User</label>
				</div>
			</div>
		</div>
		<div class="form-label-group">
			<input type="email" id="inputEmail" class="form-control"
				value="abc@gmail.com" placeholder="Email address" name="username" required autofocus />
			<label for="inputEmail">Email address</label>
		</div>

		<div class="form-label-group">
			<input type="password" id="inputPassword" class="form-control"
				value="admin@123" placeholder="Password" name="password" required /> <label
				for="inputPassword">Password</label>
		</div>

		<div class="checkbox mb-3">
			<label> <input type="checkbox" value="remember-me" />
				Remember me
			</label>
		</div>
		<button class="btn btn-lg btn-primary btn-block" type="submit">
			Sign in</button>
		<p class="mt-5 mb-3 text-muted text-center">&copy; 2018-2019</p>
	</form>
</body>
</html>
