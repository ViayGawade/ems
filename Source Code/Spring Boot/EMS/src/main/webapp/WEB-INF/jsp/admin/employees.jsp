<%@ include file="common/header.jspf"%>
<title>All Employees</title>

<style>
.actionBtn button {
	padding: 5px;
	margin: 5px;
}
</style>
</head>
<%@ include file="common/navigation.jspf"%>


<!-- START Main Body -->
<div class="pageContent">
	<h2 style="text-align: center;">Employee</h2>
	<br>
	<div class="row" style="float: right; margin-right: 10px;">
		<button type="button" class="btn btn-success btn-lg"
			data-toggle="modal" data-target="#employeeModal" data-title="Add">New
			Employee</button>
	</div>
	<br>
	<br>
	<br>
	<div class="row ">
		<table class="table table-bordered table-hover text-center"
			style="width: 85%; margin: 0 auto !important;">
			<thead class="thead-dark">
				<tr>
					<th scope="col" width="20%">First</th>
					<th scope="col" width="20%">Last</th>
					<th scope="col" width="10%">DOB</th>
					<th scope="col" width="15%">Department</th>
					<th scope="col" width="15%">Action</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Mark</td>
					<td>Otto</td>
					<td>21 Oct 1996</td>
					<td>ISSC</td>
					<td class="actionBtn">
						<button type="button" class="btn btn-info btn-sm"
							data-toggle="modal" data-target="#employeeModal"
							data-title="Update">
							<i class="fa fa-pencil"></i>&nbsp;Update
						</button>

						<button type="button" class="btn btn-danger btn-sm">
							<i class="fa fa-trash"></i>&nbsp;Delete
						</button>
					</td>
				</tr>
				<tr>
					<td>Jacob</td>
					<td>Thornton</td>
					<td>21 Oct 1996</td>
					<td>ISSC</td>
					<td class="actionBtn">
						<button type="button" class="btn btn-info btn-sm"
							data-toggle="modal" data-target="#employeeModal"
							data-title="Update">
							<i class="fa fa-pencil"></i>&nbsp;Update
						</button>

						<button type="button" class="btn btn-danger btn-sm">
							<i class="fa fa-trash"></i>&nbsp;Delete
						</button>
					</td>
				</tr>
				<tr>
					<td>Larry</td>
					<td>the Bird</td>
					<td>21 Oct 1996</td>
					<td>ISSC</td>
					<td class="actionBtn">
						<button type="button" class="btn btn-info btn-sm"
							data-toggle="modal" data-target="#employeeModal"
							data-title="Update">
							<i class="fa fa-pencil"></i>&nbsp;Update
						</button>

						<button type="button" class="btn btn-danger btn-sm">
							<i class="fa fa-trash"></i>&nbsp;Delete
						</button>
					</td>
				</tr>
			</tbody>
		</table>
	</div>


	<!-- Employee Modal -->
	<div class="modal fade" id="employeeModal" tabindex="-1" role="dialog"
		aria-labelledby="employeeModal" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<form action="" method="post" class="needs-validation" novalidate>
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLongTitle">Modal
							title</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">

						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="firstName">First Name:</label><input type="text"
									name="firstName" id="firstName" class="form-control"
									placeholder="Enter First Name" required>
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please provide a valid Name.
								</div>
								<small id="emailHelp" class="form-text text-muted">We'll
									never share your email with anyone else.</small>
							</div>
							<div class="form-group col-md-6">
								<label for="lastName">Last Name:</label><input type="text"
									name="lastName" id="lastName" class="form-control"
									placeholder="Enter Last Name" required>
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please provide a valid Name.
								</div>
								<small id="emailHelp" class="form-text text-muted">Enter
									Last Name</small>
							</div>
						</div>

						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="doj">Date of Birth:</label> <input type="text"
									name="doj" id="doj" class="form-control datepicker"
									placeholder="Enter DOB" required>
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please provide a valid Date
									of Birth.</div>
								<small id="emailHelp" class="form-text text-muted">Enter
									DOB</small>
							</div>
							<div class="form-group col-md-6">
								<label for="department">Department:</label> <select
									class="form-control" id="department" name="department" required>
									<option value="">Select Department</option>
									<option>ISSC</option>
									<option>CS</option>
									<option>IT</option>
									<option>Physics</option>
									<option>Math</option>
								</select>
								<div class="valid-feedback">Looks good!</div>
								<div class="invalid-feedback">Please select Department.</div>
								<small id="emailHelp" class="form-text text-muted">Select
									your Department</small>
							</div>
						</div>


					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-success">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<!-- End Of Employee Modal -->



	<script src="${pageContext.request.contextPath}/custom/js/employee/passDataToModal.js"></script>

	<script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function () {
            'use strict';
            window.addEventListener('load', function () {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function (form) {
                    form.addEventListener('submit', function (event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>

	<!-- Validator.js -->
	<script src="${pageContext.request.contextPath}/custom/js/validations/admin/employee.js"></script>
</div>
<!-- END Main Body -->

<%@ include file="common/footer.jspf"%>