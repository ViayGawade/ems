<%@ include file="common/header.jspf" %>
	<title>Regulation & Legislation </title>
	<style>
        .actionButton button {
            margin: 5px;
        }
    </style>
</head>
<%@ include file="common/navigation.jspf" %>


<!-- START Main Body -->
<div class="pageContent">
	<h2 style="text-align: center;">Regulation & Legislation</h2><br>
        <div class="row" style="float: right;margin-right: 10px;">
            <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#regulationModal"
                data-title="Add">New
                Regulation</button>
        </div><br><br><br>
        <div class="row table-responsive">
            <table class="table table-bordered table-hover text-center" style="width: 95%;margin: 0 auto !important;">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">RL Type</th>
                        <th scope="col">Regulation Details</th>
                        <th scope="col">Creation Date</th>
                        <th scope="col">Department</th>
                        <th scope="col">Status</th>
                        <th scope="col" width="18%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>201</td>
                        <td>Dress Code</td>
                        <td>Formal Uniform</td>
                        <td>21 Oct 1996</td>
                        <td>ISSC</td>
                        <td>Declare</td>
                        <td class="actionButton">
                            <button type="button" class="btn btn-primary btn-sm"><i
                                    class="fa fa-eye"></i>&nbsp;&nbsp;View Comments</button>&nbsp;&nbsp;
                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal"
                                data-target="#regulationModal" data-title="Update"><i
                                    class="fa fa-pencil"></i>&nbsp;Update</button>

                            <button type="button" class="btn btn-danger btn-sm"><i
                                    class="fa fa-trash"></i>&nbsp;Delete</button>
                        </td>
                    </tr>
                    <tr>
                        <td>201</td>
                        <td>Dress Code</td>
                        <td>Formal Uniform</td>
                        <td>21 Oct 1996</td>
                        <td>ISSC</td>
                        <td>Declare</td>
                        <td class="actionButton">
                            <button type="button" class="btn btn-primary btn-sm"><i
                                    class="fa fa-eye"></i>&nbsp;&nbsp;View
                                Comments</button>&nbsp;&nbsp;
                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal"
                                data-target="#regulationModal" data-title="Update"><i
                                    class="fa fa-pencil"></i>&nbsp;Update</button>
                            <button type="button" class="btn btn-danger btn-sm"><i
                                    class="fa fa-trash"></i>&nbsp;Delete</button></td>
                    </tr>
                    <tr>
                        <td>201</td>
                        <td>Dress Code</td>
                        <td>Formal Uniform</td>
                        <td>21 Oct 1996</td>
                        <td>ISSC</td>
                        <td>Declare</td>
                        <td class="actionButton">
                            <button type="button" class="btn btn-primary btn-sm"><i
                                    class="fa fa-eye"></i>&nbsp;&nbsp;View
                                Comments</button>&nbsp;&nbsp;
                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal"
                                data-target="#regulationModal" data-title="Update"><i
                                    class="fa fa-pencil"></i>&nbsp;Update</button>
                            <button type="button" class="btn btn-danger btn-sm"><i
                                    class="fa fa-trash"></i>&nbsp;Delete</button></td>
                    </tr>
                </tbody>
            </table>
        </div>
        
        <!-- Regulation Modal -->
    <div class="modal fade" id="regulationModal" tabindex="-1" role="dialog" aria-labelledby="regulationModal"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <form action="" method="post" class="needs-validation" novalidate>
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="departmentID">Department ID:</label>
                                <input type="text" name="departmentID" id="departmentID" class="form-control"
                                    value="System will genrate" readonly required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="RLType">RL Type:</label>
                                <select class="form-control" id="RLType" name="RLType" required>
                                    <option value="">Select RL Type</option>
                                    <option>Dress</option>
                                    <option>Working Timing</option>
                                    <option>Holiday</option>
                                    <option>Exam</option>
                                    <option>Student</option>
                                    <option>Other</option>
                                </select>
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                                <div class="invalid-feedback">
                                    Please select RL Type.
                                </div>
                                <small id="emailHelp" class="form-text text-muted">Select category of Regulation</small>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="department">Department:</label>
                                <select class="form-control" id="department" name="department" required>
                                    <option value="">Select Department</option>
                                    <option>ISSC</option>
                                    <option>CS</option>
                                    <option>IT</option>
                                    <option>Physics</option>
                                    <option>Math</option>
                                </select>
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                                <div class="invalid-feedback">
                                    Please select Department.
                                </div>
                                <small id="emailHelp" class="form-text text-muted">Select your
                                    Department</small>
                            </div>
                        </div>

                        <div class="form-row">
                            <label for="RLDetails">Regulation Details:</label>
                            <textarea name="RLDetails" id="RLDetails" cols="30" rows="2" placeholder="Write Regulation Details here.." required class="form-control"></textarea>
                           
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                            <div class="invalid-feedback">
                                Please provide a Regulation Details.
                            </div>
                            <small id="emailHelp" class="form-text text-muted">Provide detailed information about
                                Regulation.</small>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="creationDate">Creation Date:</label>
                                <input type="text" name="creationDate" id="creationDate" class="form-control datepicker"
                                    placeholder="Enter DOB" required>
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                                <div class="invalid-feedback">
                                    Please provide a valid Creation Date.
                                </div>
                                <small id="emailHelp" class="form-text text-muted">Enter Creation Date</small>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="status">Status:</label>
                                <select class="form-control" id="status" name="status" required>
                                    <option value="">Select Status</option>
                                    <option>Create</option>
                                    <option>Pending</option>
                                    <option>Approved</option>
                                    <option>Other</option>
                                </select>
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                                <div class="invalid-feedback">
                                    Please select Status.
                                </div>
                                <small id="emailHelp" class="form-text text-muted">Select status of Regulation.</small>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- End Of Regulation Modal -->



    <script src="${pageContext.request.contextPath}/custom/js/regulation/passDataToModal.js"></script>

    <script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function () {
            'use strict';
            window.addEventListener('load', function () {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function (form) {
                    form.addEventListener('submit', function (event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>

    <!-- Validator.js -->
    <script src="${pageContext.request.contextPath}/custom/js/validations/admin/regulation.js"></script>
        
</div>
<!-- END Main Body -->

<%@ include file="common/footer.jspf" %>