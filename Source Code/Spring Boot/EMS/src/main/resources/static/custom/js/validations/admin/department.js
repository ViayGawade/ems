//Validate Deparment Name size
function validateDepartmentName(thisObj) {
    let fieldValue = thisObj.val();
    if (fieldValue.length > 2 && fieldValue.length<80) {
        $(thisObj).addClass('is-valid');
        return true;
    } else {
        $(thisObj).addClass('is-invalid');
        return false;
    }
    
}    

//On every :input focusin remove existing validation messages if any
$(':input').click(function () {

    $(this).removeClass('is-valid is-invalid');

});

// On every :input focusin remove existing validation messages if any
$(':input').keydown(function () {

    $(this).removeClass('is-valid is-invalid');

});

function validateAllFormField() {
	deptNameInput=$(".addDepartmentForm input[name='name']");
	return validateDepartmentName(deptNameInput);
}

$( "form" ).submit(function( event ) {  
	//Remove Whitespace from Forms Input Fields
	 $("input[type='text']").each(function(){
		    $(this).val($.trim($(this).val()));
		  });
});  

